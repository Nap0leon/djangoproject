# Django crash course

[The course](https://www.youtube.com/watch?v=D6esTdOLXh4)

## What is Django ?

- Python Web Framework
- Free & Open Source
- Rapid Dev
- Model-Template-View Design pattern

## Framework ?

- Good for beginners
- Less freeom but less chances of screwing up #TheDjangoWay

## Why Django

- Lot of core feature
- High performance
- Very secure
- Versatile
- Scalable

## MVC Django

Model : Data access layer - Anything to do with interacting, relating and validating the data

Template : Presentation layer

View : Business logic layer - Accesses the model and displays the appropriate template (Sort of bridge between model & template)

## Dependancies

- Python
- pip
- virtualenvwrapper
- mysql
- mysqlclient
- mamp


`urls.py` : Routing module
`settings.py` : Many settings such as databases, middleware, domain name...,


### Start web server

`$python manage.py runserver`

**You have 13 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.**

### Setting database

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'djangoproject',
        'USER': '',
        'PASSWORD': '',
        'HOST': '/Applications/MAMP/tmp/mysql/mysql.sock',
        'PORT': '3306'
    }
}
```

### Migrate models

```python
class Posts(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    created_at = models.DateTimeField(default=datetime.now)
```

Then run in command : `python manage.py makemigrations <models>`

Will generate an X_initial.py file which can lead to create the table in the database.

Run command : `python manage.py migrate`